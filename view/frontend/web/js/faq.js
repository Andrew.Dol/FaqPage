require([
        'jquery',
        'jquery/ui',
        'jquery/validate'
    ], function ($) {

    initialization();
    var form = $("#questionForm"),
        submited = $("#submited"), //Block submit message
        textInForm = $("#questionForm input, #questionForm textarea");

    /**
     * Init:
     *     accordion,
     *     line event,
     *     dialog window,
     *     validate form
     */

    form.validate();

    function initialization(){

        $('.accordion').accordion({
            collapsible: true,
            active: false,
            icons: false,
            heightStyle: 'content'
        });

        appearanceLine();
        openDialog();
    }

    /**
     *  Show submit message and hide form
     */

    form.submit(function() {
        if( $(this).valid() ) {
            form.css('display', 'none');
            submited.css('display', 'block');
            $(textInForm).val(""); // delete text from input and textarea

        }
        return false;
    });

    /**
     * Show and hide line after accordion title
     */

    function appearanceLine(){

        $(".title").on("click", function () {

            var element = $(this).children(".line");        // line block
            var width = parseInt(element.css("width"));     // width line now 

            if(width > 2){                                  // Show line after title
                element.css("width", 0 + "px");
                setTimeout(function () {
                    element.css("border", "none");
                    element.css("height", "2px");
                }, 700);
            }
            else{
                element.css("border", "1px solid #57b487"); // Hide line after title
                element.css("height", "0");
                element.css("width", 65 + "px");
            }
        });
    }

    /**
     * Init jquery.ui dialog,
     * opens button
     */

    function openDialog() {
        var dialog = $( "#dialog" );
        dialog.dialog({
            autoOpen: false,
            dialogClass: 'my-dialog',
            width: '80%'
        });

        $("#question").on( "click", function() {
            form.css('display', 'block');
            submited.css('display', 'none');
            $(textInForm).val("");
            dialog.dialog( "open" );
        });
    }
});

